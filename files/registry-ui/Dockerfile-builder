FROM golang:stretch as builder
ENV GOPATH /opt
ENV GO111MODULE on
RUN apt-get update && \
    apt-get install -y ca-certificates git bash gcc musl-dev
RUN mkdir -p /opt/src/github.com/quiq/docker-registry-ui && \
    git clone https://github.com/Quiq/docker-registry-ui.git /opt/src/github.com/quiq/docker-registry-ui
WORKDIR /opt/src/github.com/quiq/docker-registry-ui
# RUN git checkout 1f6801acfbd76cf31d30ab36eb9c1545190f20f1 && git clean -fdx
RUN sed -i 's|github.com/labstack/echo.*|github.com/labstack/echo v3.3.9|' go.mod
RUN go test -v ./registry && \
    go build -a -v -o /opt/docker-registry-ui *.go

# build: -race is only supported on linux/amd64, linux/ppc64le, freebsd/amd64, netbsd/amd64, darwin/amd64 and windows/amd64

FROM debian:stretch-slim
WORKDIR /opt
RUN apt-get update && \
    apt-get install -y ca-certificates && \
    mkdir /opt/data
COPY --from=builder /opt/src/github.com/quiq/docker-registry-ui/templates /opt/templates
COPY --from=builder /opt/src/github.com/quiq/docker-registry-ui/static /opt/static
COPY --from=builder /opt/docker-registry-ui /opt/
COPY ./config.yml /opt/config.yml
# USER nobody
ENTRYPOINT ["/opt/docker-registry-ui"]
