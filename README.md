Role Name
=========

An Ansible role to install Docker-Registy with UI behind an Apache httpd with basic-authentification, SSL and WebDAV.

Requirements
------------

Docker installed and service has to be activated. User should be able to use docker-compose. Certificate should be available too.

Install role from Git server with _ansible-galaxy_.

requirements.yml :

```yaml
---
- src: ssh://code/dev/ansible_role_docker_registry_plus.git
  scm: git
  version: origin/master
  name: docker_registry_plus
```

Galayx install :

```shell
ansible-galaxy install -r requirements.yml --roles-path roles
```

Role Variables
--------------

| Name                     | Default                           |
|--------------------------|-----------------------------------|
| bearer_token             | -                                 |
| default_user             | -                                 |
| default_password         | -                                 |
| docker_compose_project   | docker-registry-ui                |
| ssl_cert                 | ~/.ssl/{{ server_name }}/cert.pem |
| ssl_key                  | ~/.ssl/{{ server_name }}/key.pem  |
| server_name              | -                                 |
| server_admin             | -                                 |

Dependencies
------------

No other roles required.

Example Playbook
----------------

```yaml
---
- hosts:
    - registry.local

  vars_files:
    -  ~/ansible_vars_docker_registry_plus.yml

  roles:
    - role: docker_registry_plus
      vars:
        bearer_token: 12345678901234567890
        default_password: password
        default_user: user
        server_admin: admin@registry.local
        server_name: registry.local
        ssl_cert: "~/.ssl/{{ server_name }}/cert.pem"
        ssl_key: "~/.ssl/{{ server_name }}/key.pem"
```

Author Information
------------------

fwalk___gitlab

References
----------

- https://docs.docker.com/registry/
- https://github.com/Quiq/docker-registry-ui
- https://hub.docker.com/r/apcheamitru/arm32v7-registry/
- https://httpd.apache.org/docs/2.4/
- https://lobradov.github.io/Building-docker-multiarch-images/
